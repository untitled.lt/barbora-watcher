const rp = require('request-promise-native');
const get = require('lodash.get');
const { notify } = require('./notify');
require('dotenv').config();

const requestOptions = {
    method: 'get',
    uri: 'https://www.barbora.lt/api/eshop/v1/cart/deliveries',
    headers: {
        Authorization: `Basic ${process.env.AUTH_HASH}`,
        Cookie: process.env.COOKIE,
    },
    json: true,
};

const createSummary = ({ hasFreeSlots, result }) => {
    if (!hasFreeSlots) {
        return 'No free slots found.';

    } else {
        const summaryDays = [];
        result.days
            .filter(day => day.dayHasFreeSlots)
            .forEach(({ day, freeHours }) => {
                const hours = freeHours
                    .map(({ hour }) => hour.replace(/ /g, ''))
                    .join(', ');
                summaryDays.push(`${day}: ${hours} val.`);
            });

        return 'Free time slots: ' + summaryDays.join(', ');
    }
};

const doParse = json => {
    const result = {
        days: [],
        stats: {
            days: {
                free: 0,
                total: 0,
            },
            hours: {
                free: 0,
                total: 0,
            }
        }
    };
    const days = get(json, 'deliveries[0].params.matrix') || [];
    let hasFreeSlots = false;

    days.forEach(({ day, hours = [] }) => {
        const freeHours = [];

        hours.forEach(({ hour, available, price }) => {
            result.stats.hours.total++;

            if (available) {
                freeHours.push({ hour, available, price });
                result.stats.hours.free++;
            }
        });

        result.stats.days.total++;

        if (freeHours.length) {
            hasFreeSlots = true;
            result.stats.days.free++;
        }

        result.days.push({
            day,
            stats: `${freeHours.length}/${hours.length} free`,
            dayHasFreeSlots: !!freeHours.length,
            ...(freeHours.length ? { freeHours } : {}),
        });
    });

    return {
        hasFreeSlots,
        summary: createSummary({ hasFreeSlots, result }),
        ...result,
    };
};


const doRequest = async () => {
    let result;

    try {
        // const resp = require('./resp');
        const resp = await rp(requestOptions);
        result = doParse(resp);
    } catch (e) {
        return console.error('Failed to parse response', e);
    }


    if (result.hasFreeSlots) {
        await notify(result);
    } else {
        // console.log(JSON.stringify(result, null, 4))
        const freeHours = get(result, 'stats.hours.free');
        const totalHours = get(result, 'stats.hours.total');
        console.log(`${Date().toString().slice(0, 24)}: ${freeHours}/${totalHours} free slots :(`);
    }
};

doRequest();
setInterval(doRequest, process.env.CHECK_INTERVAL_SECONDS * 1000);
