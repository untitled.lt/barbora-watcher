const rp = require('request-promise-native');
require('dotenv').config();

const notificationMinInterval = 60 * 1000 * process.env.NOTIFY_INTERVAL_MINUTES;
let lastNotificationTime = 0;

const getRequestOptions = ({
    title,
    message,
    priority = 1,
    sound = 'persistent'
}) => {
    return {
        method: 'post',
        uri: 'https://api.pushover.net/1/messages.json',
        body: {
            token: process.env.PUSHOVER_TOKEN,
            user: process.env.PUSHOVER_USER,
            message,
            title,
            sound,
            priority,
        },
        json: true,
    };
};

const notify = async result => {
    console.log('!!! FREE SLOTS !!!');

    if (Date.now() - lastNotificationTime > notificationMinInterval) {
        lastNotificationTime = Date.now();
        console.log(JSON.stringify(result, null, 4));
        const rpOptions = getRequestOptions({
            title: 'Barbora free slots!!!',
            message: result.summary
        });
        await rp(rpOptions)
            .catch(console.error);
    }
};

module.exports = { notify };
